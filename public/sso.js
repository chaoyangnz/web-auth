var WENJIU_AUTH_BASE='//auth.wenjiu.io'

// addEventListener support for IE8
function bindEvent(element, eventName, eventHandler) {
  if (element.addEventListener){
      element.addEventListener(eventName, eventHandler, false);
  } else if (element.attachEvent) {
      element.attachEvent('on' + eventName, eventHandler);
  }
}
// Listen to message from child window
bindEvent(window, 'message', function (e) {
  if(e.data) {
    try {
      var auth = JSON.parse(e.data)
      if(auth.sso) {
        localStorage.setItem('auth', JSON.stringify(auth));
      }
    } catch (e) {}
  }
});

var iframeSource = WENJIU_AUTH_BASE + '/sso.iframe.html';
// Create the iframe
var iframe = document.createElement('iframe');
iframe.setAttribute('src', iframeSource);
iframe.setAttribute('id', 'sso_iframe');
iframe.style.width = 0;
iframe.style.height = 0;
iframe.style.border = 0;
iframe.style.border = 'none';

window.onload = function() {
  document.body.appendChild(iframe);
}
