FROM nginx:stable

USER root
COPY dist /opt/web/
COPY nginx.conf /etc/nginx/
