import Vue from 'vue';
import Element from 'element-ui';
import Countdown from './Countdown.vue';
import Header from './Header.vue';

// register all element-ui components
Vue.use(Element /* , { size: 'small' } */);

// register app components globally
Vue.component('wj-countdown', Countdown);
Vue.component('wj-header', Header);
