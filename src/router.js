import Vue from 'vue';
import Router from 'vue-router';
import Login from './views/Login.vue';
import Signup from './views/Signup.vue';
import Activate from './views/Activate.vue';
import { Loading } from 'element-ui';

Vue.use(Router);

const router = new Router({
  mode: 'history',
  routes: [
    {
      path: '/login',
      name: 'Login',
      component: Login,
    },
    {
      path: '/signup',
      name: 'Signup',
      component: Signup,
    },
    {
      path: '/activate',
      name: 'Activate',
      component: Activate,
    },
  ],
});

router.beforeEach((to, from, next) => {
  router.$loading = Loading.service({fullscreen: true});
  next();
});

router.afterEach((to, from, next) => {
  setTimeout(() => {
    if (router.$loading) {
      router.$loading.close();
      delete router.$loading;
    }
  }, 0);
  window.scrollTo(0, 0);
});

export default router;
